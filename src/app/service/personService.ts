import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as globals from './globals';

@Injectable({
    providedIn: 'root'
  })
  
  export class PersonService {
  
    constructor(private httpClient: HttpClient) {}

    getPeople(url) {
        return this.httpClient.get(url);
    }

    getPerson(id) {
        return this.httpClient.get(globals.getPeople + id);
    }

  }