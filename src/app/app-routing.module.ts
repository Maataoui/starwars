import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PeopleListComponent } from './view/people-list/people-list.component';
import { PeopleDetailComponent } from './view/people-detail/people-detail.component';


const routes: Routes = [
  {path: '', component: PeopleListComponent},
  {path: 'people', component: PeopleListComponent},
  {path: 'people/:id', component: PeopleDetailComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
