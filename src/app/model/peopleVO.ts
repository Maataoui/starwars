export interface PeopleVO {

    id: number;
    birthYear: any;
    gender: string;
    eyeColor: string;
}