import { Component, OnInit } from '@angular/core';
import { PersonService } from '../../service/personService';

@Component({
  selector: 'app-people-list',
  templateUrl: './people-list.component.html',
  styleUrls: ['./people-list.component.scss']
})
export class PeopleListComponent implements OnInit {
    public people;
    public page;
    public selectedNumber = 6;
    public count;
    public term;
    public next;
    public previous;

    constructor(private personService: PersonService) { }

    ngOnInit(): void {
        this.getPeople("http://swapi.dev/api/people/");
    }

    getPeople(url) {
        this.personService.getPeople(url)
            .subscribe((response : any) => {
                this.people = response.results;
                this.count = response.count;
                this.previous = response.previous;
                this.next = response.next;
            })
    }

    onNext() {
        this.people = [];
        this.getPeople(this.next);
    }

    onPrev() {
        this.getPeople(this.previous);
    }

}
