import { Component, OnInit } from '@angular/core';
import { PersonService } from '../../service/personService';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-people-detail',
    templateUrl: './people-detail.component.html',
    styleUrls: ['./people-detail.component.scss']
})
export class PeopleDetailComponent implements OnInit {
    public person;
    public id;

    constructor(private personService: PersonService,
                private route: ActivatedRoute) { 
                    this.id = this.route.snapshot.params.id;
                }

    ngOnInit(): void {
        this.getPerson(this.id);
    }

    getPerson(id) {
        this.personService.getPerson(id)
        .subscribe((response : any) => {
            this.person = response;
        })
    }

}
