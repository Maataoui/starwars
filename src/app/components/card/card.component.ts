import { Component, OnInit, Input } from '@angular/core';
import { PeopleVO } from '../../model/peopleVO';
import { Router } from '@angular/router';

@Component({
    selector: 'app-card',
    templateUrl: './card.component.html',
    styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
    public _person: any;

    constructor(private router: Router) { }

    @Input()
    public set person(value: PeopleVO) {
        this._person = value;
    }

    ngOnInit(): void {
    }

    getpersonDetail(personUrl) {
        let id = personUrl.substr(personUrl.length - 2);
        this.router.navigateByUrl('people/' + id + '/');
    }

}
